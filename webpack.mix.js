let mix = require('laravel-mix');

mix.setPublicPath('dist')
  .sass('scr/scss/main.scss', 'css')
  .options({
    processCssUrls: true,
    clearConsole: true
  })
  .js('src/js/app.js', 'js')
  .browserSync({
    proxy: null,
    server: {
      base: './'
    },
    notify: true,
    files: [
      './*.html',
      'dist/js/**/*.js',
      'dist/css/**/*.css',
    ]
  });